import 'package:flutter/material.dart';
import 'MainF.dart';
import 'Register.dart';
import 'SignIn.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sign Up Screen ',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      initialRoute: 'SignIn',
      routes: {
        'SignUp':(context)=>Register(),
        'SignIn':(context)=>SignIn(),
        'Home':(context)=> MainForm(),
      },
    );
  }
}

